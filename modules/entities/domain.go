package entities

import (
	"github.com/gofiber/fiber/v2"
)

type UserRepository interface {
	CreateUser(user *User) (*User, error)
	GetUsers()([]User, error)
	GetUser(username string, password string) (*User, error)

	BlackListToken(tokenblacklist *TokenBlacklist) (*TokenBlacklist, error)
}

type UserService interface {
	UserCreated(userReq *UserRequest) (int, fiber.Map)
	UserGets()(int,fiber.Map)
	Login(username string, password string) (int,fiber.Map)
	BlackListToken(tokenblacklist *TokenBlacklist) (*TokenBlacklist, error)
}
