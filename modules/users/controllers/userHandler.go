package controllers

import (
	"fmt"
	"os"
	"strings"
	"time"
	"userservice/modules/entities"
	"userservice/modules/logs"

	"github.com/gofiber/fiber/v2"
	"go.uber.org/zap"
	"github.com/golang-jwt/jwt/v4"
)

type userHandler struct {
	userService entities.UserService
}

func NewUserHandler(r fiber.Router, userService entities.UserService) {

	controller := &userHandler{userService: userService}

	r.Post("/register", controller.CreateUser)
	r.Post("/login", controller.Login)
	r.Post("/logout", controller.Logout)
	r.Get("/getUsers", controller.GetUsers)

}

func (h *userHandler) CreateUser(c *fiber.Ctx) error {
	var userRequestBody entities.UserRequest
	if err := c.BodyParser(&userRequestBody); err != nil {
		logs.Info("Invalid request to Create User :", zap.Error(err))
		return c.Status(fiber.StatusBadRequest).JSON(fiber.Map{
			"message": "Invalid request",
		})
	}

	codeStatus, response := h.userService.UserCreated(&userRequestBody)
	return c.Status(codeStatus).JSON(response)
}

func (h *userHandler) GetUsers(c *fiber.Ctx) error {
	codeStatus, response := h.userService.UserGets()
	return c.Status(codeStatus).JSON(response)
}

func (h *userHandler) Login(c *fiber.Ctx) error {
	var RequestBody entities.LoginRequest
	if err := c.BodyParser(&RequestBody); err != nil {
		logs.Info("Invalid request to Login :", zap.Error(err))
		return c.Status(fiber.StatusBadRequest).JSON(fiber.Map{
			"message": "Invalid request",
		})
	}
	codeStatus, response := h.userService.Login(RequestBody.Username, RequestBody.Password)
	return c.Status(codeStatus).JSON(response)
}

func (h *userHandler) Logout(c *fiber.Ctx) error {
	authHeader := c.Get("Authorization")
	if authHeader == "" {
		return c.Status(fiber.StatusUnauthorized).JSON(fiber.Map{
			"message": "Unauthorized: Missing Bearer Token",
		})
	}

	token := extractBearerToken(authHeader)
	if token == "" {
		return c.Status(fiber.StatusUnauthorized).JSON(fiber.Map{
			"message": "Unauthorized: Invalid Bearer Token",
		})
	}

	// Add the token to the blacklist with an expiration time equal to the token's remaining time
	validToken, err := validateToken(token)
	if err != nil {
		return c.Status(fiber.StatusUnauthorized).JSON(fiber.Map{
			"message": fmt.Sprintf("Unauthorized: %s", err.Error()),
		})
	}

	exp := validToken.Claims.(jwt.MapClaims)["exp"].(float64)
	expirationTime := time.Unix(int64(exp), 0)

	blacklistToken := entities.TokenBlacklist{
		Token:     token,
		ExpiresAt: expirationTime,
	}

	tokenRepo, err := h.userService.BlackListToken(&blacklistToken)
	if err != nil {
		logs.Info("Can't insert BlackListToken", zap.String("error", err.Error()))

		return c.JSON(fiber.Map{
			"message": "an't insert BlackListToken",
			"token": tokenRepo.Token,
		})
	}

	return c.JSON(fiber.Map{
		"message": "Successfully logged out",
	})
}

func extractBearerToken(authHeader string) string {
	if len(authHeader) > 7 && strings.ToUpper(authHeader[0:6]) == "BEARER" {
		return authHeader[7:]
	}
	return ""
}

func validateToken(tokenString string) (*jwt.Token, error) {
	token, err := jwt.Parse(tokenString, func(token *jwt.Token) (interface{}, error) {
		if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
			return nil, fmt.Errorf("unexpected signing method: %v", token.Header["alg"])
		}
		return []byte(os.Getenv("JWT_SECRET")), nil
	})
	if err != nil {
		return nil, err
	}

	if token.Valid {
		return token, nil
	} else {
		return nil, fmt.Errorf("invalid token")
	}
}

func isTokenExpired(token *jwt.Token) bool {
	claims, ok := token.Claims.(jwt.MapClaims)
	if !ok {
		return true
	}

	exp, ok := claims["exp"].(float64)
	if !ok {
		return true
	}

	expirationTime := time.Unix(int64(exp), 0)

	return time.Now().After(expirationTime)
}
