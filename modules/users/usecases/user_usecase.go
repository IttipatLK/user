package usecases

import (
	"fmt"
	"os"
	"time"
	"userservice/configs"
	"userservice/modules/entities"
	"userservice/modules/logs"

	"github.com/gofiber/fiber/v2"
	"github.com/golang-jwt/jwt/v4"
	"github.com/joho/godotenv"
	"go.uber.org/zap"
)

type userService struct {
	useRepo entities.UserRepository
	cfg     *configs.Configs
}

func NewUserService(useRepo entities.UserRepository, cfg *configs.Configs) entities.UserService {
	// Load dotenv config
	if err := godotenv.Load("../config.env"); err != nil {
		panic(err.Error())
	}
	return &userService{
		useRepo: useRepo,
		cfg:     cfg,
	}
}

func (s *userService) UserCreated(user *entities.UserRequest) (int, fiber.Map) {
	userCreate := entities.User{
		Username:    user.Username,
		Password:    user.Password,
		Name:        user.Name,
		Description: user.Description,
		UserImage:   user.UserImage,
	}
	if userCreate.UserImage == "" {
		userCreate.UserImage = "https://cdn-icons-png.flaticon.com/512/149/149071.png"
	}

	created, err := s.useRepo.CreateUser(&userCreate)
	if err != nil {
		logs.Info("Can't create user", zap.String("error", err.Error()))

		return fiber.ErrBadRequest.Code, fiber.Map{
			"message": fmt.Sprintf("BadRequest : %v", err),
		}
	}

	userIdResponse := created.ID

	return fiber.StatusOK, fiber.Map{
		"message":    "user successfully created.",
		"TaskId":     userIdResponse,
		"data":       created,
		"status":     "OK",
		"statusCode": 200,
	}

}

func (s *userService) UserGets() (int, fiber.Map) {
	users, err := s.useRepo.GetUsers()
	if err != nil {
		logs.Info("Can't get user", zap.String("error", err.Error()))

		return fiber.ErrBadRequest.Code, fiber.Map{
			"message": fmt.Sprintf("BadRequest : %v", err),
		}
	}
	usersResponse := []entities.UserRes{}
	for _, item := range users {
		Response := entities.UserRes{
			UserID:      int(item.ID),
			Username:    item.Username,
			Password:    item.Password,
			Name:        item.Name,
			Description: item.Description,
			UserImage:   item.UserImage,
		}
		usersResponse = append(usersResponse, Response)
	}

	return fiber.StatusOK, fiber.Map{
		"message":    "Get User successfully",
		"data":       usersResponse,
		"status":     "OK",
		"statusCode": 200,
	}
}

func (s *userService) BlackListToken(tokenblacklist *entities.TokenBlacklist) (*entities.TokenBlacklist, error) {
	blacklist, err := s.useRepo.BlackListToken(tokenblacklist)
	if err != nil {
		logs.Info("Can't insert blacklist token", zap.String("error", err.Error()))
		return nil, err
	}
	return blacklist, nil

}

func (s *userService) Login(username string, password string) (int, fiber.Map) {
	user, err := s.useRepo.GetUser(username, password)
	if err != nil {
		logs.Info("Can't get user", zap.String("error", err.Error()))

		return fiber.ErrBadRequest.Code, fiber.Map{
			"message": fmt.Sprintf("BadRequest : %v", err),
		}
	}

	token, err := generateJWT(*user)
	if err != nil {
		logs.Info("Can't generate token", zap.String("error", err.Error()))

		return fiber.ErrInternalServerError.Code, fiber.Map{
			"message": fmt.Sprintf("InternalServerError : %v", err),
		}
	}

	if valid, err := validateToken(token); err != nil || !valid {
		logs.Info("Invalid token", zap.String("error", err.Error()))

		return fiber.ErrInternalServerError.Code, fiber.Map{
			"message": fmt.Sprintf("InternalServerError : %v", err),
		}
	}

	return fiber.StatusOK, fiber.Map{
		"message":    "Login successfully",
		"token":      token,
		"status":     "OK",
		"statusCode": 200,
	}

}

func generateJWT(user entities.User) (string, error) {

	claims := jwt.MapClaims{
		"authorized": true,
		"userId":     user.ID,
		"username":   user.Username,
		"name":       user.Name,
		"userImage":  user.UserImage,
		"exp":        time.Now().Add(time.Hour * 2).Unix(),
		// "exp":        time.Now().Add(time.Minute * 1).Unix(),
	}

	// สร้าง Token โดยใช้ claims และคีย์
	token := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)

	// สร้าง JWT string จาก Token
	tokenString, err := token.SignedString([]byte(os.Getenv("JWT_SECRET")))
	if err != nil {
		return "", err
	}

	return tokenString, nil
}

func validateToken(tokenString string) (bool, error) {

	token, err := jwt.Parse(tokenString, func(token *jwt.Token) (interface{}, error) {
		if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
			return nil, fmt.Errorf("unexpected signing method: %v", token.Header["alg"])
		}
		return []byte(os.Getenv("JWT_SECRET")), nil
	})
	if err != nil {
		return false, err
	}

	if _, ok := token.Claims.(jwt.MapClaims); ok && token.Valid {
		return true, nil
	} else {
		return false, nil
	}
}
